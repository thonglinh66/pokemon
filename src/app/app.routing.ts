import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { PokemonLayoutComponent } from './layouts/pokemon-layout/pokemon-layout.component';

const routes: Routes =[
  {
    path: '',
    redirectTo: 'games',
    pathMatch: 'full',
  }, {
    path: '',
    component: PokemonLayoutComponent,
    children: [{
      path: '',
      loadChildren: () => import('./layouts/pokemon-layout/pokemon-layout.module').then(m => m.PokemonLayoutModule)
    }]
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
       useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
