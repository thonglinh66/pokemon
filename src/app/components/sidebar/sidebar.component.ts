import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/games', title: 'Games',  icon: 'videogame_asset', class: '' },
    { path: '/generations', title: 'Generations',  icon:'bubble_chart', class: '' },
    { path: '/locations', title: 'Locations',  icon:'location_on', class: '' },
    { path: '/items', title: 'Items',  icon:'sd_storage', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
