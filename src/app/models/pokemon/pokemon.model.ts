export class Pokemon {
    id?: any;
    name?: string;
    height?: number;
    order?: number;
    weight?: number;
    types?: Types[];
  }

  export class Types {
    id?: any;
    slot?: number;
    type?: Type;
  }

  export class Type {
    id?: any;
    name?: string;
    url?: string
  }