import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {PokemonLayoutRoutes } from './pokemon-layout.routing';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import { GamesComponent } from 'app/games/games.component';
import { GenerationsComponent } from 'app/generations/generations.component';
import { LocationsComponent } from 'app/locations/locations.component';
import { ItemsComponent } from 'app/items/items.component';
import { CarouselModule} from 'primeng/carousel';
import {DataViewModule} from 'primeng/dataview';
import {RatingModule} from 'primeng/rating';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PokemonLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    CarouselModule,
    DataViewModule,
    RatingModule,
    DialogModule,
    ButtonModule,
    TabViewModule
  ],
  declarations: [
    GamesComponent,
    GenerationsComponent,
    LocationsComponent,
    ItemsComponent,

  ]
})

export class PokemonLayoutModule {
}
