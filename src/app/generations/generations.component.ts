import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../models/pokemon/pokemon.model';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-generations',
  templateUrl: './generations.component.html',
  styleUrls: ['./generations.component.css']
})
export class GenerationsComponent implements OnInit {

  displayModal: boolean = false;
  pokemons: Pokemon[] = [];
  pokemon: Pokemon = new Pokemon;
  

  constructor(private pokemonService: PokemonService) {
     
  }
  getInforPoke(id: number) {
    this.displayModal = true;
    this.pokemonService.getInfor(id).subscribe((res:any)=>{
      this.pokemon = res;
    });  
  }

  getAllPokemon(){
    this.pokemonService.getAll().subscribe((res:any)=>{
      this.pokemons = res.results
      console.log(this.pokemons);
      
    })
  }
  ngOnInit():void {
      this.getAllPokemon();
  }

}
