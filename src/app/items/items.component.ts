import { Component, OnInit } from '@angular/core';
import { Item } from '../models/item/item.model';
import { ItemService } from '../services/item.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  displayModal: boolean = false;
  items: Item[] = [];
  item: Item = new Item;
  

  constructor(private itemService: ItemService) {
     
  }
  getAllItem(){
    this.itemService.getAll().subscribe((res:any)=>{
      this.items = res.results
      
    })
  }
  ngOnInit():void {
      this.getAllItem();
  }

}
