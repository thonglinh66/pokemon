import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Item } from '../models/item/item.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'Application/json' }),
};
const apiUrl = 'https://pokeapi.co/api/v2/item';

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  constructor(private httpClient: HttpClient) {}

  getAll(): Observable<Item[]> {
    return this.httpClient.get<Item[]>(apiUrl).pipe();
  }

  getInfor(id: number): Observable<Item> {
    return this.httpClient.get<Item>(`${apiUrl}/${id}`).pipe();
  }
}
