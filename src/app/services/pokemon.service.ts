import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Pokemon } from '../models/pokemon/pokemon.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'Application/json' }),
};
const apiUrl = 'https://pokeapi.co/api/v2/pokemon';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  constructor(private httpClient: HttpClient) {}

  getAll(): Observable<Pokemon[]> {
    return this.httpClient.get<Pokemon[]>(apiUrl).pipe();
  }

  getInfor(id: number): Observable<Pokemon> {
    return this.httpClient.get<Pokemon>(`${apiUrl}/${id}`).pipe();
  }
}
