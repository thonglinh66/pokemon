import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as Chartist from 'chartist';
import { Pokemon } from '../models/pokemon/pokemon.model';
import { PokemonService } from '../services/pokemon.service';
import { Item } from '../models/item/item.model';
import { ItemService } from '../services/item.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  video; 
  displayModal: boolean = false;
  responsiveOptions;
  pokemons: Pokemon[] = [];
  pokemon: Pokemon = new Pokemon;
  pokeItems: Item[] = [];
  

  constructor(public sanitizer: DomSanitizer, private pokemonService: PokemonService, private itemService: ItemService) {
      this.responsiveOptions = [{
          breakpoint: '1024px',
          numVisible: 1,
          numScroll: 3
      }];
  }
  getInforPoke(id: number) {
    this.displayModal = true;
    this.pokemonService.getInfor(id).subscribe((res:any)=>{
      this.pokemon = res;
    });  
  }

  getAllPokemon(){
    this.pokemonService.getAll().subscribe((res:any)=>{
      this.pokemons = res.results
    })
  }

  getAllItem(){
    this.itemService.getAll().subscribe((res:any)=>{
      this.pokeItems = res.results
      
    })
  }
  ngOnInit():void {
      this.getAllPokemon();
      this.getAllItem();
      this.video = [
        {random: 'video1', src: this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/D0zYJ1RQ-fs')},
        {random: 'video2', src: this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/bILE5BEyhdo')},
        {random: 'video3', src: this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/1roy4o4tqQM')},
        {random: 'video4', src: this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/uBYORdr_TY8')},
      ];
  }

}
